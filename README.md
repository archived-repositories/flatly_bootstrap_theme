
## Flatly bootstrap theme

[Boostrap](https://github.com/twbs/bootstrap) theme based on [Bootswatch](https://bootswatch.com)'s [Flatly theme](https://bootswatch.com/flatly/).

## Overrides

- Overridden gigantic Flatly headings.
- Annihilated web font import.
- Overridden sans serif fonts (the default is now **Open Sans**) and monospace fonts (the default is now **DejaVu Sans Mono**).

## Added styles

- Text styling classes:
    - **text-bold**
    - **text-bolder**
    - **text-italic**
    - **text-oblique**
    - **text-overline**
    - **text-line-through**
    - **text-underline**
    - **text-font-size-small**
    - **text-font-size-medium**
    - **text-font-size-large**
    - **text-font-size-x-large**
    - **text-font-size-xx-large**
- Class to center images horizontally:
    - **img-centered-container**: This class should be set to a `<div>` containing an `<img>`.
- Miscellaneous tweaks/classes:
    - **div.boxed**: A class to "frame" an element with a border with radius and a shadow.
    - Added a bottom border to `<h1>` and `<h2>` tags and to `h1` and `h2` classes.
    - Tweaked `<pre>` to allow word wrapping. Also changed the background color and added a border with radius to add contrast against the page background.

## Build theme from source

The **build.py** script is the one that performs the CSS files building from the sources.

This repository needs to be *deep cloned* to checkout the needed [Boostrap](https://github.com/twbs/bootstrap) and [Bootswatch](https://github.com/thomaspark/bootswatch) sub-modules to be able to build the CSS files from source.

```bash
# Deep clone repository.
# --shallow-submodules option is to avoid donwloding hundreds of megabytes of
# the sub-modules history (bootstrap and bootswatch).
git clone --recurse-submodules --shallow-submodules git_repository_url
```

If the parsing of the SASS file isn't needed/wanted, the already parsed CSS files found inside the **dist** folder can be used instead. These CSS files are already auto-prefixed.

To be able to parse the `flatly_bootstrap_theme.scss` file, [Dart Sass](https://github.com/sass/dart-sass/releases) needs to be available in the system PATH or the `sassc` command needs to be available (can be installed from any distribution package manager).

The generated CSS files will have no vendor prefixes. An attempt to auto-prefix the generated CSS files will be made. Node.js and the `postcss-cli` and `autoprefixer` node modules are needed to perform this task.

```shell
sudo npm install -g postcss-cli autoprefixer
```

Or use whatever method/command you are willing to endure to auto-prefix the generated CSS files.
